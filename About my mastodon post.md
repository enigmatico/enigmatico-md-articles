# About my Mastodon post...

I'm talking about this post: [enter link description here](https://niu.moe/@robots/100140419549787183)

This post has got a lot of attention, being one of my most popular posts in Mastodon. Which is fine by me. Unfortunately, people did not get the true meaning of this post. The content of this post is the following:

> Twitter was not bad because "hashtags this" or "trends that". Twitter was bad because:
	
	1. Centralized.
	2. They forced you to ID yourself with a phone number.
	3. They hand your data to third party companies.
	4. Most governments were using it to ID people saying bad things about them.
	5. Ads and corporative campaigns.

Of course, since Mastodon is a decentralized service which is used as an alternative, people is quick into assuming this is a direct criticism to Twitter as a centralized service. However, this is not the case. Let me explain the background about this post:

At the start of this month (June 2018), Gargron (which is the creator of Mastodon) decided to add a new feature to Mastodon, which was basically a "Hashtag trending" feature, similar to Twitter, in which users would see which tags were more trendy than others. A pretty harmless feature that could be used to show people what was going on in the fediverse. Nothing more, nothing less. Of course it had it's flaws and it didn't take long for Gargron and most people to notice that bot accounts could exploit this feature to promote their own tags, which was quickly fixed with a new algorithm later on.

However, a group of people in the fediverse (which is the AP network as a whole), didn't like this feature. And they started their own hate campaign against it. Their reason for this? Well... **twitter had it, so it is bad**. Literally, that was their main argument. Many people switched from Twitter to Mastodon/Pleroma to avoid their corporative practises, but they forgot what makes Twitter a bad platform and instead, charge against anything that resembles twitter without realizing that Mastodon is, in fact, an alternative to Twitter (so it has some of it's features, of course).

So what happens after that is that this group of people started making a ruckus about the feature. They started blaming Gargron, telling him that he did it without their consent, then they started literally insulting him when he ignored them and eventually, there was a whole drama in both GitHub and Mastodon and eventually, this people decided to start the 'Fork Mastodon' campaign which as far as I know, didn't have too much success up to this point. All for a stupid feature, and because Twitter had it.

It's at this point when I decided to write this post.  People was being too stupid, literally. The feature was harmless and could be helpful to some people that might want to see what is going on, but there was a lot of people complaining about it simply because it was something that Twitter had. People left Twitter, they hate it, and they hate anything that resembles it (not realizing they are still using an alternative to it that is based on it).

**But what makes Twitter so bad after all?** Why did we leave Twitter? Clearly it isn't because of the trending topics, right? So, I decided to write a post to remember people why they should hate Twitter. Twitter is bad simply because it's centralized, it's a corporation, it has corporative practises, and it sells your data to third party companies in order to make money. That's the issue with twitter and that's why some people decentralized it, right?

Is it bad because it has hashtags? No. Is it bad because it has trends? No! Twitter "per se" is **NOT BAD**. The core of the product is good! And people likes it. It's their corporative practises what makes us hate it!

So, I decided to write down a small post on Mastodon about it. To remind people about why we abandoned twitter and why we are abandoning every centralized network. And to allow and consent any features that can be good.

Unfortunately, people took it out of it's context unknowingly, and they think this is a direct criticism to Twitter. No it's not! It's a critic to YOU, because YOU forgot what makes Twitter a bad platform!

And now I am writting these lines, in hopes they all understand...

Oh, and by the way. The feature got reduced to a simply activity graph when searching for tags.
 
